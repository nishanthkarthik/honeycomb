#pragma once

#include <string>

namespace CLI {
enum class CommandMode {
    listTargets,
    listArchs,
    compile,
    help,
};

struct Config {
    CommandMode mCommandMode;
    std::string mTarget;
    std::string mArchitecture;
    std::string mInputFile;
    std::string mOutputFile;
    std::string mResourceName;
    bool mPrintDefines;

    Config(int argc, char *argv[]);
};
}
