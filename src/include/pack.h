#pragma once

#include <string>
#include <vector>

namespace CLI {
struct Config;
}

namespace Pack {

class BFD {
public:
    std::vector<std::string> targets() const;
    std::vector<std::string> architectures() const;
    void compile(CLI::Config config) const;

private:
    static struct Init {
        Init();
    } m_init;
};
}
