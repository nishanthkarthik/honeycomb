#include "cli.h"
#include "pack.h"

#include <algorithm>
#include <iostream>
#include <string>

namespace {
void printItem(const std::string &string) { std::cout << string << std::endl; }
}

int main(int argc, char *argv[])
{
    const auto cli = CLI::Config(argc, argv);
    Pack::BFD bfd;
    switch (cli.mCommandMode) {
    case CLI::CommandMode::listTargets: {
        const auto targets = bfd.targets();
        std::for_each(targets.begin(), targets.end(), printItem);
        break;
    }
    case CLI::CommandMode::listArchs: {
        const auto targets = bfd.architectures();
        std::for_each(targets.begin(), targets.end(), printItem);
        break;
    }
    case CLI::CommandMode::compile: {
        bfd.compile(cli);
        break;
    }
    default:
        return EXIT_FAILURE;
    }
}
