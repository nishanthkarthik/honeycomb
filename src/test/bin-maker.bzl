def _impl(ctx):
    args = ctx.actions.args()
    args.add(ctx.outputs.file)
    args.add(ctx.attr.size)
    ctx.actions.run(
        inputs = [ctx.executable.tool],
        outputs = [ctx.outputs.file],
        executable = ctx.executable.tool,
        arguments = [args],
        progress_message = "Generating blob {}".format(ctx.outputs.file),
    )
    return [
        DefaultInfo(
            files = depset([ctx.outputs.file]),
            runfiles = ctx.runfiles([ctx.outputs.file]),
        ),
    ]

gen_blob = rule(
    implementation = _impl,
    attrs = {
        "size": attr.int(),
        "file": attr.output(mandatory = True),
        "tool": attr.label(executable = True, cfg = "exec"),
    },
)
