#include "clipp.h"
#include <fstream>
#include <iostream>

#ifndef RESOURCE_NAME
#error RESOURCE_NAME macro is not defined
#endif

#ifndef RESOURCE_SIZE
#error RESOURCE_SIZE macro is not defined
#endif

#ifndef SYM_SIZE
#error SYM_SIZE macro is not defined
#endif

#ifndef SYM_DATA
#error SYM_DATA macro is not defined
#endif

#define expect(X)                                                                                                      \
    if (!(X))                                                                                                          \
        throw std::runtime_error                                                                                       \
        {                                                                                                              \
            std::string("Expectation failed: ") + #X                                                                   \
        }

extern "C" {
extern uint32_t SYM_SIZE;
extern uint8_t SYM_DATA[RESOURCE_SIZE];
}

int main(int argc, char *argv[])
{
    std::string resourceFileName;
    const auto cli
        = clipp::group(clipp::value("resource-file-name", resourceFileName) % "Source resource to compare with");
    if (!clipp::parse(argc, argv, cli)) {
        std::cerr << clipp::make_man_page(cli, "linktest") << std::endl;
        std::cerr << "Got";
        for (int i = 1; i < argc; ++i) {
            std::cerr << " " << argv[i];
        }
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }

    std::ifstream resourceFile { resourceFileName, std::ios::binary | std::ios::ate };
    expect(resourceFile.good());
    size_t length = resourceFile.tellg();
    resourceFile.seekg(std::ios::beg);
    expect(length == SYM_SIZE);
    expect(length == RESOURCE_SIZE);

    std::vector<uint8_t> data(length);
    resourceFile.read(reinterpret_cast<char *>(data.data()), data.size());

    std::vector<uint8_t> originalData(SYM_DATA, SYM_DATA + SYM_SIZE);
    expect(data == originalData);

    std::cout << "Test complete" << std::endl;
    return EXIT_SUCCESS;
}
