#include "clipp.h"
#include <fstream>
#include <iostream>
#include <limits>
#include <random>

#define SEED 0xAD

int main(int argc, char *argv[])
{
    std::string outputFilePath;
    size_t size {};
    const auto cli = (clipp::value("output-file-name", outputFilePath), clipp::value("size-bytes", size));
    if (!clipp::parse(argc, argv, cli)) {
        std::cerr << clipp::make_man_page(cli, "bin-maker") << std::endl;
        return EXIT_FAILURE;
    }

    // Need a predicable sequence
    std::mt19937 engine { SEED };
    std::uniform_int_distribution<int> distribution { std::numeric_limits<uint8_t>::min(),
        std::numeric_limits<uint8_t>::max() };

    std::vector<uint8_t> data(size);
    for (auto &item : data) {
        item = static_cast<uint8_t>(distribution(engine));
    }

    std::ofstream outputFile { outputFilePath, std::ios::binary | std::ios::trunc };
    outputFile.write(reinterpret_cast<const char *>(data.data()), data.size());
    return EXIT_SUCCESS;
}
