load("@rules_cc//cc:find_cc_toolchain.bzl", "find_cc_toolchain")

def _impl(ctx):
    args = ctx.actions.args()
    args.add("compile")
    args.add("--input", ctx.file.input)
    args.add("--name", ctx.attr.name)
    args.add("--target", ctx.attr.target)
    args.add("--architecture", ctx.attr.architecture)
    args.add("--output", ctx.outputs.output)
    args.add("--no-print-defines")
    ctx.actions.run(
        outputs = [ctx.outputs.output],
        inputs = [ctx.file.input, ctx.executable.tool],
        executable = ctx.executable.tool,
        arguments = [args],
        progress_message = "Generating resource {}".format(ctx.outputs.output),
    )
    toolchain = ctx.toolchains["@bazel_tools//tools/cpp:toolchain_type"].cc
    (obj_linking_context, linking_outputs) = cc_common.create_linking_context_from_compilation_outputs(
        name = ctx.attr.name,
        actions = ctx.actions,
        feature_configuration = cc_common.configure_features(
            ctx = ctx,
            cc_toolchain = toolchain,
        ),
        cc_toolchain = toolchain,
        compilation_outputs = cc_common.create_compilation_outputs(
            objects = depset([ctx.outputs.output]),
        ),
    )
    linker_input = cc_common.create_linker_input(
        owner = ctx.label,
        libraries = depset([linking_outputs.library_to_link]),
    )
    linking_context = cc_common.create_linking_context(
        linker_inputs = depset([linker_input]),
    )
    return [
        DefaultInfo(files = depset([
            ctx.outputs.output,
            linking_outputs.library_to_link.static_library,
        ])),
        CcInfo(linking_context = linking_context),
    ]

compile_resource = rule(
    implementation = _impl,
    attrs = {
        "input": attr.label(mandatory = True, allow_single_file = True),
        "target": attr.string(mandatory = True),
        "architecture": attr.string(mandatory = True),
        "tool": attr.label(executable = True, cfg = "exec"),
        "output": attr.output(mandatory = True),
    },
    toolchains = ["@rules_cc//cc:toolchain_type"],
    provides = [DefaultInfo, CcInfo],
    fragments = ["cpp"],
)
