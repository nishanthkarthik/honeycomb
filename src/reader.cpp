#include "bfd.h"

#include <bitset>
#include <cassert>
#include <iostream>

constexpr auto cl_bin = R"(C:\Users\Kooldex\CLionProjects\honeycomb\reference\ref.obj)";
constexpr auto gcc_bin = R"(C:\Users\Kooldex\CLionProjects\honeycomb\reference\ref.o)";
constexpr auto test_bin = R"(C:\Users\Kooldex\CLionProjects\honeycomb\reference\test.obj)";

std::ostream &operator<<(std::ostream &out, asymbol *symbol)
{
    out << symbol->name << ":\t" << std::hex << symbol->value << ",\t";
    out << "FL: " << symbol->flags << ",\t";
    out << "SEC: " << symbol->section->name << ",\t";
    return out;
}

std::ostream &operator<<(std::ostream &out, asection *section)
{
    out << section->name << "\t" << section->size << "\t" << std::hex << section->flags;
    return out;
}

int main()
{
    bfd_init();
    bfd *abfd = bfd_openr(test_bin, "pe-x86-64");
    auto open_error = bfd_get_error();

    bool isCorrectFormat = bfd_check_format(abfd, bfd_object);
    auto error = bfd_get_error();
    assert(isCorrectFormat);

    // -------------------------- Target -------------------------------------
    std::cout << "Target " << abfd->xvec->name << std::endl;

    // --------------------------- Symbols -----------------------------------
    size_t symsize = bfd_get_symtab_upper_bound(abfd);
    auto **symbols = static_cast<asymbol **>(malloc(symsize));

    int symcount = bfd_canonicalize_symtab(abfd, symbols);
    assert(symcount > 0);

    std::cout << "Symbols" << std::endl;
    for (int i = 0; i < symcount; ++i) {
        std::cout << '\t' << symbols[i] << std::endl;
    }

    // --------------------------- Sections -----------------------------------
    asection *section = abfd->sections;
    std::cout << std::endl
              << "Sections" << std::endl;

    while (section != nullptr && section->next != nullptr) {
        std::cout << '\t' << ((section->flags & SEC_DATA) ? '+' : ' ') << ((section->flags & SEC_READONLY) ? '+' : ' ') << section << std::endl;
        section = section->next;
    }

    bfd_close(abfd);
    return 0;
}
