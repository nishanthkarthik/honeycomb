set(BFD_VERSION 2.39)

include(ExternalProject)

set(ROOT ${CMAKE_CURRENT_BINARY_DIR}/BFDProject)

set(BFD_INCLUDE_DIR ${ROOT}/include)
set(BFD_BINARY_DIR ${ROOT}/src/BFDProject-build)
set(BFD_LIBRARY_DIRS
    ${BFD_BINARY_DIR}/bfd
    ${BFD_BINARY_DIR}/zlib
    ${BFD_BINARY_DIR}/libiberty
)

file(MAKE_DIRECTORY ${BFD_INCLUDE_DIR} ${BFD_LIBRARY_DIRS})

# Find the TARGET triple
execute_process(
    COMMAND ${CMAKE_C_COMPILER} -dumpmachine
    OUTPUT_VARIABLE TARGET_TRIPLE
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

# From binutils' perspective: To build bfd for windows,
# BUILD = x86_64-pc-linux-gnu (inferred)
# HOST = x86_64-w64-mingw32
# TARGET = x86_64-w64-mingw32
#
# This is called a cross-native build https://stackoverflow.com/a/21992287/3951920
ExternalProject_Add(
    BFDProject
    URL https://ftp.gnu.org/gnu/binutils/binutils-${BFD_VERSION}.tar.gz
    URL_HASH SHA256=d12ea6f239f1ffe3533ea11ad6e224ffcb89eb5d01bbea589e9158780fa11f10
    PREFIX ${ROOT}
    CONFIGURE_COMMAND
        ${ROOT}/src/BFDProject/configure
        --prefix=<INSTALL_DIR>
        --host=${TARGET_TRIPLE}
        --target=${TARGET_TRIPLE}
        --disable-plugins
        --disable-nls
        --without-system-zlib
        --enable-64-bit-bfd
        --with-pic
        --enable-static
        --disable-shared
        --enable-targets=all
        --disable-binutils
        --disable-gas
        --disable-gold
        --disable-gprof
        --disable-gprofng
        --disable-intl
        --disable-ld
        --disable-libctf
        --disable-opcodes
        --disable-texinfo
        CC=${CMAKE_C_COMPILER}
        CFLAGS=${CMAKE_C_FLAGS_RELEASE}
        CXX=${CMAKE_CXX_COMPILER}
        CXXFLAGS=${CMAKE_CXX_FLAGS_RELEASE}
    BUILD_COMMAND ${MAKE}
)

add_library(BFD INTERFACE)
add_dependencies(BFD BFDProject)

target_include_directories(BFD INTERFACE
    ${BFD_INCLUDE_DIR}      # bfd.h
    ${BFD_BINARY_DIR}       # bfd/config.h
)
target_link_directories(BFD INTERFACE ${BFD_LIBRARY_DIRS})
target_link_libraries(BFD INTERFACE bfd z iberty)

# To know if we actually use the libraries we built now
target_link_options(BFD INTERFACE -Wl,--trace)
