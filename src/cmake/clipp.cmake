include(ExternalProject)

ExternalProject_Add(
    ClippProject
    GIT_REPOSITORY https://github.com/muellan/clipp.git
    GIT_TAG 2c32b2f1f7cc530b1ec1f62c92f698643bb368db
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/ClippProject
    CMAKE_ARGS
        -DCMAKE_BUILD_TYPE=Release
        -DCMAKE_MESSAGE_LOG_LEVEL=WARNING
        -DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>
        -DBUILD_TESTING=OFF
)

ExternalProject_Get_Property(ClippProject install_dir)
add_library(clipp INTERFACE)
add_dependencies(clipp ClippProject)
target_include_directories(clipp INTERFACE ${install_dir}/include)
