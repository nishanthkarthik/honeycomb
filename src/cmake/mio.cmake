include(ExternalProject)

ExternalProject_Add(
    MIOProject
    GIT_REPOSITORY https://github.com/mandreyel/mio.git
    GIT_TAG 3f86a95c0784d73ce6815237ec33ed25f233b643
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/MIOProject
    CMAKE_ARGS
        -DCMAKE_BUILD_TYPE=Release
        -DCMAKE_MESSAGE_LOG_LEVEL=WARNING
        -DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>
        -Dmio.tests=OFF
)

ExternalProject_Get_Property(MIOProject SOURCE_DIR)
add_library(mio INTERFACE)
add_dependencies(mio MIOProject)
target_include_directories(mio INTERFACE ${SOURCE_DIR}/single_include)