#include "cli.h"

#include "clipp.h"
#include <iostream>

namespace CLI {
Config::Config(int argc, char **argv)
    : mCommandMode { CommandMode::help }
    , mPrintDefines { true }
{
    using namespace clipp;
    const auto help = (command("help").set(mCommandMode, CommandMode::help));

    const auto compile = (command("compile").set(mCommandMode, CommandMode::compile),
        (required("--input", "-i") & value("resource", mInputFile)) % "Input resource file to pack",
        (required("--name", "-n") & value("resource-name", mResourceName)) % "Symbol prefix in the binary",
        (required("--target", "-t") & value("target-name", mTarget)) % "Target name of the output binary",
        (required("--architecture", "-a") & value("architecture-name", mArchitecture))
            % "Architecture of the output binary",
        (required("--output", "-o") & value("output", mOutputFile)) % "Output binary to link against",
        (option("--no-print-defines").set(mPrintDefines, false) % "Hide macros printing resource symbol names"));

    const auto archs = command("list-archs").set(mCommandMode, CommandMode::listArchs) % "Lists all architectures";
    const auto targets = command("list-targets").set(mCommandMode, CommandMode::listTargets) % "Lists all targets";
    const auto cli = (compile | archs | targets);

    if (!parse(argc, argv, cli) || mCommandMode == CommandMode::help) {
        std::cerr << make_man_page(cli, "honeycomb") << std::endl;
        std::exit(EXIT_FAILURE);
    }
}
}