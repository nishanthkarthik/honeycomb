#include "bfd.h"

#include <array>
#include <memory>
#include <stdexcept>
#include <vector>

#include "mio/mio.hpp"

#include "cli.h"
#include "pack.h"

#define check(X)                                                                                                       \
    [&] {                                                                                                              \
        const auto val = (X);                                                                                          \
        if (!val) {                                                                                                    \
            bfd_perror("Error");                                                                                       \
            throw std::runtime_error("Error at '" + std::string(#X) + "'");                                            \
        } else {                                                                                                       \
            return val;                                                                                                \
        }                                                                                                              \
    }()

namespace {
std::vector<std::string> fromCStrArray(std::unique_ptr<const char *> ptr)
{
    std::vector<std::string> items;
    for (auto it = ptr.get(); it && *it; ++it) {
        items.emplace_back(*it);
    }
    return items;
}

struct BFDClose {
    void operator()(bfd *abfd)
    {
        if (abfd) {
            bfd_close(abfd);
        }
    }
};

// Assumes octet == byte
template <typename T, size_t N = sizeof(T)> std::array<uint8_t, N> toTargetEndian(T t, bfd *abfd)
{
    std::array<uint8_t, N> array;
    static_assert(N <= 8, "T should be maximum 64 bits");
    bfd_put(8 * N, abfd, static_cast<bfd_vma>(t), array.data());
    return array;
}
}

namespace Pack {
std::vector<std::string> BFD::targets() const
{
    return fromCStrArray(std::unique_ptr<const char *>(bfd_target_list()));
}

std::vector<std::string> BFD::architectures() const
{
    return fromCStrArray(std::unique_ptr<const char *>(bfd_arch_list()));
}

void BFD::compile(CLI::Config config) const
{
    const auto dataSymbolName = config.mResourceName + "_data";
    const auto sizeSymbolName = config.mResourceName + "_size";

    std::array<asymbol *, 3> symbols {};
    const auto resource = mio::mmap_source(config.mInputFile);

    using SizeField = uint32_t;
    const SizeField resourceSize = resource.size();
    const SizeField totalSize = sizeof(resourceSize) + resourceSize;

    const auto abfd
        = std::unique_ptr<bfd, BFDClose>(check(bfd_openw(config.mOutputFile.c_str(), config.mTarget.c_str())));
    check(bfd_set_format(abfd.get(), bfd_object));

    const auto archInfo = check(bfd_scan_arch(config.mArchitecture.c_str()));
    bfd_set_arch_info(abfd.get(), archInfo);

    // https://www.airs.com/blog/archives/518
    // Mark stack as non executable
    {
        const auto targetInfo = bfd_find_target(config.mTarget.c_str(), abfd.get());
        if (targetInfo->flavour == bfd_target_elf_flavour)
            check(bfd_make_section_with_flags(abfd.get(), ".note.GNU-stack", SEC_NO_FLAGS));
    }

    const auto sectionFlags = SEC_ALLOC | SEC_LOAD | SEC_DATA | SEC_HAS_CONTENTS | SEC_READONLY;
    auto *dataSec = check(bfd_make_section_with_flags(abfd.get(), ".data", sectionFlags));

    if (config.mPrintDefines)
        printf("extern uint32_t %s;\n", sizeSymbolName.c_str());

    auto *sizeSym = check(bfd_make_empty_symbol(abfd.get()));
    sizeSym->name = sizeSymbolName.c_str();
    sizeSym->section = dataSec;
    sizeSym->flags = BSF_GLOBAL;
    sizeSym->value = 0;

    if (config.mPrintDefines)
        printf("extern uint8_t %s[%u];\n", dataSymbolName.c_str(), resourceSize);

    auto *dataSym = check(bfd_make_empty_symbol(abfd.get()));
    dataSym->name = dataSymbolName.c_str();
    dataSym->section = dataSec;
    dataSym->flags = BSF_GLOBAL;
    dataSym->value = sizeof(SizeField);

    symbols = { sizeSym, dataSym, nullptr };
    check(bfd_set_symtab(abfd.get(), symbols.data(), symbols.size() - 1));

    // Section contents are after writing symbol table
    check(bfd_set_section_size(dataSec, totalSize));

    // Set endianness to target
    // Little : target elf64-x86-64     architecture i386:x86-64
    // Big    : target elf64-bigaarch64 architecture aarch64
    const auto targetResourceSize = toTargetEndian(resourceSize, abfd.get());
    check(bfd_set_section_contents(abfd.get(), dataSec, targetResourceSize.data(), 0, targetResourceSize.size()));

    // Endianness does not matter here
    check(bfd_set_section_contents(abfd.get(), dataSec, resource.data(), sizeof(resourceSize), resource.size()));
}

BFD::Init::Init() { check(bfd_init() == BFD_INIT_MAGIC); }
}
