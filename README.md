# honeycomb

honeycomb is a resource compiler for embedding binary blobs inside your application.

# Goals

honeycomb is intended to be a lightweight tool. It does not support the features provided by a full blown resource system, like Qt's. honeycomb can run on any host platform to compile resources for any target platform and architecture.

# Usage

## Command line

```
honeycomb compile
    --input <resource> 
    --name <resource-name> --output <output>
    --target <target-name> --architecture <architecture-name> 

honeycomb list-archs
honeycomb list-targets
```

## Sample usage on windows

```
honeycomb.exe compile --input foo.png --name foo_resource --target pe-x86-64 --architecture i386:x86-64 --output foo-resource.obj
```

This command generates `foo-resource.obj` you can link to. You can directly pass it to the compiler driver or set up an [Object library](https://cmake.org/cmake/help/latest/command/add_library.html#id3) target in CMake. It also prints the symbols you need to embed in your C or C++ source,

```c
extern uint32_t foo_resource_size;
extern uint8_t foo_resource_data[2480];
```

If your compiler mangles these symbols in a C++ source file, enclose them with an `extern "C"` block.

# Acknowledgements

honeycomb is thankful for the decades of work that went into [GNU binutils](https://www.gnu.org/software/binutils/). It makes extensive use of [clipp](https://github.com/muellan/clipp), a command line parsing library and [mio](https://github.com/mandreyel/mio), a memory-mapped file IO library.

