def _impl(rctx):
    rctx.report_progress("Downloading {}".format(rctx.attr.url))
    rctx.download_and_extract(
        url = rctx.attr.url,
        sha256 = rctx.attr.sha256,
        stripPrefix = rctx.attr.prefix,
    )
    rctx.template(
        "BUILD",
        Label("@//toolchain:compiler.bzl.tpl"),
        substitutions = {
            "{prefix}": rctx.attr.prefix,
        },
    )

external_cc_toolchain = repository_rule(
    implementation = _impl,
    attrs = {
        "url": attr.string(),
        "sha256": attr.string(),
        "prefix": attr.string(),
    },
)

def make_filegroups(prefix):
    [native.filegroup(
        name = "{}_group".format(it),
        srcs = [it, "@{}//:{}".format(prefix, it)],
    ) for it in [
        "ar",
        "as",
        "cpp",
        "dwp",
        "gcc",
        "gcov",
        "nm",
        "objcopy",
        "strip",
    ]]

    # ld invokes gcc
    native.filegroup(
        name = "ld_group",
        srcs = ["ld", "@{}//:ld".format(prefix), ":gcc_group"],
    )

    native.filegroup(
        name = "all_wrappers",
        srcs = native.glob(include=["*"], exclude=["*.bzl", "BUILD"]),
    )

    native.filegroup(
        name = "all_files",
        srcs = ["@{}//:all_files".format(prefix), ":all_wrappers"],
    )
