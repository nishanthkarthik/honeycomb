load("@bazel_tools//tools/cpp:cc_toolchain_config_lib.bzl", "artifact_name_pattern", "tool_path")
load(":features.bzl", "all_flags")

PREFIX = "x86_64-w64-mingw32"

def _impl(ctx):
    tool_paths = [tool_path(name = it, path = "{}/{}".format(ctx.attr.wrapper_root, it)) for it in [
        "objcopy",
        "objdump",
        "ar",
        "strip",
        "ld",
        "gcov",
        "gcc",
        "nm",
        "cpp",
    ]]

    return cc_common.create_cc_toolchain_config_info(
        ctx = ctx,
        toolchain_identifier = PREFIX,
        host_system_name = "x86_64-linux-gnu",
        target_system_name = "x86_64-w64-mingw32",
        target_cpu = "x86_64-w64-mingw32",
        target_libc = "mingw32",
        compiler = "gcc-8.5.0",
        abi_version = "gcc-8.5.0",
        abi_libc_version = "mingw32-9.0",
        tool_paths = tool_paths,
        features = all_flags(ctx.attr),
        artifact_name_patterns = [
            artifact_name_pattern(
                category_name = "executable",
                prefix = "",
                extension = ".exe",
            ),
        ],
    )

cc_x86_64_w64_mingw32_config = rule(
    implementation = _impl,
    attrs = {
        "wrapper_root": attr.string(),
        "optimize": attr.bool(default = False),
    },
    provides = [CcToolchainConfigInfo],
)
