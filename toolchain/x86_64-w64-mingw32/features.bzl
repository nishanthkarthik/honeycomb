load("@bazel_tools//tools/build_defs/cc:action_names.bzl", "ACTION_NAMES")
load("@bazel_tools//tools/cpp:cc_toolchain_config_lib.bzl", "feature", "flag_group", "flag_set", "tool_path")

COMPILE_ACTIONS = [
    ACTION_NAMES.c_compile,
    ACTION_NAMES.cpp_compile,
    ACTION_NAMES.cpp_header_parsing,
]

LINK_ACTIONS = [
    ACTION_NAMES.cpp_link_executable,
    ACTION_NAMES.cpp_link_dynamic_library,
    ACTION_NAMES.cpp_link_nodeps_dynamic_library,
]

def compile_flags():
    return feature(
        name = "compile_flags",
        enabled = True,
        flag_sets = [
            flag_set(
                actions = COMPILE_ACTIONS,
                flag_groups = [
                    flag_group(flags = [
                        "-g",
                        "-Wno-builtin-macro-redefined",
                        "-no-canonical-prefixes",
                        '-D__DATE__="redacted"',
                        '-D__TIMESTAMP__="redacted"',
                        '-D__TIME__="redacted"',
                        "-Wall",
                        "-fPIC",
                    ]),
                ],
            ),
        ],
    )

def link_flags():
    return feature(
        name = "link_flags",
        enabled = True,
        flag_sets = [
            flag_set(
                actions = LINK_ACTIONS,
                flag_groups = [
                    flag_group(flags = [
                        "-pass-exit-codes",
                        "-static",
                    ]),
                ],
            ),
        ],
    )

def optimize_flags(optimize):
    return feature(
        name = "optimize_flags",
        enabled = optimize,
        flag_sets = [flag_set(
            actions = COMPILE_ACTIONS,
            flag_groups = [
                flag_group(flags = [
                    "-g0",
                    "-O2",
                    "-DNDEBUG",
                    "-ffunction-sections",
                    "-fdata-sections",
                    "-U_FORTIFY_SOURCE",
                ]),
            ],
        )],
    )

def thread_flags():
    return feature(
        name = "threads",
        enabled = False,
        flag_sets = [flag_set(
            actions = LINK_ACTIONS,
            flag_groups = [
                flag_group(flags = ["-lpthread"]),
            ],
        )],
    )

def cpp_flags():
    return feature(
        name = "cpp",
        enabled = False,
        flag_sets = [
            flag_set(
                actions = COMPILE_ACTIONS,
                flag_groups = [
                    flag_group(flags = [
                        "-std=c++17",
                    ]),
                ],
            ),
            flag_set(
                actions = LINK_ACTIONS,
                flag_groups = [
                    flag_group(flags = ["-lstdc++"]),
                ],
            ),
        ],
    )

def all_flags(attr):
    return [
        compile_flags(),
        link_flags(),
        optimize_flags(attr.optimize),
        thread_flags(),
        cpp_flags(),
    ]
