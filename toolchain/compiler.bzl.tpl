package(default_visibility = ["//visibility:public"])

exports_files(glob(["bin/*"]))

filegroup(
    name = "binutils",
    srcs = glob(["{prefix}/bin/*"]),
)

filegroup(
    name = "ar",
    srcs = ["bin/{prefix}-ar"],
)

filegroup(
    name = "as",
    srcs = ["bin/{prefix}-as"],
)

filegroup(
    name = "dwp",
    srcs = [],
)

COMMON_C_CC = [":includes", ":libexec_files", ":binutils"]

filegroup(
    name = "cpp",
    srcs = ["bin/{prefix}-cpp"] + COMMON_C_CC,
)

filegroup(
    name = "gcc",
    srcs = ["bin/{prefix}-gcc"] + COMMON_C_CC,
)

filegroup(
    name = "gcov",
    srcs = ["bin/{prefix}-gcov"],
)

filegroup(
    name = "ld",
    srcs = ["bin/{prefix}-ld"],
)

filegroup(
    name = "nm",
    srcs = ["bin/{prefix}-nm"],
)

filegroup(
    name = "objcopy",
    srcs = ["bin/{prefix}-objcopy"],
)

filegroup(
    name = "objdump",
    srcs = ["bin/{prefix}-objdump"],
)

filegroup(
    name = "strip",
    srcs = ["bin/{prefix}-strip"],
)

filegroup(
    name = "includes",
    srcs = glob([
        "lib/gcc/{prefix}/*/include/**",
        "{prefix}/include/c++/*/**",
        "{prefix}/sysroot/**/include/**",
    ]),
)

filegroup(
    name = "libraries",
    srcs = glob([
        "{prefix}/sysroot/**/lib/**",
        "{prefix}/sysroot/**/lib64/**",
        "{prefix}/lib/**",
        "lib/**",
    ]),
)

filegroup(
    name = "compiler_files",
    srcs = [":as", ":gcc", ":includes"],
)

filegroup(
    name = "linker_files",
    srcs = [":ar", ":ld", ":libraries"],
)

filegroup(
    name = "libexec_files",
    srcs = glob(["libexec/**"]),
)

filegroup(
    name = "all_files",
    srcs = [
        ":compiler_files",
        ":dwp",
        ":gcov",
        ":linker_files",
        ":nm",
        ":objcopy",
        ":objdump",
        ":strip",
        ":libexec_files",
    ],
)
