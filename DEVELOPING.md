# Development dependencies

Unix based environment with the following dependencies

- CMake
- GCC
- Make

CMake should fetch the rest of the dependencies and build them when you build `honeycomb`.
