load("@rules_cc//cc:defs.bzl", "cc_library")

cc_library(
    name = "mio",
    hdrs = ["single_include/mio/mio.hpp"],
    strip_include_prefix = "single_include",
    visibility = ["//visibility:public"],
)
