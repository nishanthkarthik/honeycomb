load("@rules_foreign_cc//foreign_cc:defs.bzl", "cmake")

filegroup(
    name = "all_srcs",
    srcs = glob(include = ["**"], exclude = ["*.bzl"]),
)

ZLIB_CMAKE_VARS = {
    "BUILD_SHARED_LIBS": "OFF",
    "SKIP_BUILD_EXAMPLES": "ON",
}

cmake(
    name = "zlib",
    generate_args = ["-GNinja"],
    cache_entries = select({
        "@platforms//os:windows": dict(ZLIB_CMAKE_VARS, CMAKE_SYSTEM_NAME="Windows"),
        "@platforms//os:linux": dict(ZLIB_CMAKE_VARS, CMAKE_SYSTEM_NAME="Linux"),
    }),
    lib_source = ":all_srcs",
    out_static_libs = ["libz.a"],
    install = True,
    visibility = ["//visibility:public"],
)
