load("@rules_foreign_cc//foreign_cc:defs.bzl", "configure_make")

filegroup(
    name = "all_files",
    srcs = glob(["**"]),
)

configure_make(
    name = "binutils",
    configure_options = [
        "--disable-plugins",
        "--disable-nls",
        "--with-system-zlib",
        "--enable-64-bit-bfd",
        "--with-pic",
        "--enable-deterministic-archives",
        "--enable-relro",
        "--enable-install-libiberty",
        "--enable-static",
        "--disable-shared",
        "--enable-targets=all",
        "--disable-binutils",
        "--disable-gas",
        "--disable-gold",
        "--disable-gprof",
        "--disable-intl",
        "--disable-gdb",
        "--disable-gdbserver",
        "--disable-ld",
        "--disable-libctf",
        "--disable-readline",
        "--disable-sim",
        "--disable-libdecnumber",
        "--disable-opcodes",
        "--disable-texinfo",
        "--disable-gprofng",
    ] + select({
        "@platforms//os:linux": ["--host=x86_64-pc-linux-gnu"],
        "@platforms//os:windows": ["--host=x86_64-w64-mingw32"],
    }),
    deps = ["@zlib//:zlib"],
    defines = [
        "PACKAGE",
        "PACKAGE_VERSION",
    ],
    env = {
        "CFLAGS": "-I$$EXT_BUILD_DEPS/zlib/include -L$$EXT_BUILD_DEPS/zlib/lib",
        "MAKEFLAGS": "-j$$(nproc)",
    },
    lib_source = ":all_files",
    out_include_dir = "include",
    out_lib_dir = ".",
    out_static_libs = [
        "lib/libbfd.a",
    ] + select({
        "@platforms//os:linux": ["lib64/libiberty.a"],
        "@platforms//os:windows": ["lib/libiberty.a"],
    }),
    visibility = ["//visibility:public"],
)
