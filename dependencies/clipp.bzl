load("@rules_cc//cc:defs.bzl", "cc_library")

cc_library(
    name = "clipp",
    hdrs = ["include/clipp.h"],
    strip_include_prefix = "include",
    visibility = ["//visibility:public"],
)
